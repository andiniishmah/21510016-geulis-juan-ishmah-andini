import React,{Component} from 'react';
import YoutubeComp from '../../component/YoutubeComp/YoutubeComp';
import Product from '../product/product';

const styleHeader = {
    color: 'blue',
    fontSize:'18px',
    marginLeft: '20px'
  };

class home extends Component {
    render() {
        return (
            <div>
                <p style={styleHeader}>Youtube Components</p>
                <hr />
            <YoutubeComp time="7.12" title="title1" desc="10x ditonton. 1 hari yang lalu"/>
            <YoutubeComp time="8.12"title="title2" desc="20x ditonton. 2 hari yang lalu"/>
            <YoutubeComp time="9.12" title="title3" desc="30x ditonton. 3 hari yang lalu"/>
            <YoutubeComp time="10.12"title="title4" desc="40x ditonton. 4 hari yang lalu"/>
            <YoutubeComp />
                <p style={styleHeader}>Counter</p>
                <hr />
                <Product />
            </div>
        )
    }
}

export default home;