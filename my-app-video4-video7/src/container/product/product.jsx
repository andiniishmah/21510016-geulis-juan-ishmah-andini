import React, { Fragment,Component } from 'react';
import CardProduct from '../cardProduct/cardProduct';
import './product.css'

class product extends Component {
    state = {
        order: 4
    }
    handleCounterChange = (newValue) => {
        this.setState({
            order : newValue
        })
    }
    render () {
        return (
            <Fragment>
                <div className="card">
                    <div className="header">
                        <div className="logo">
                            <img src="https://i.ytimg.com/vi/VX0ox3uQHQc/hqdefault.jpg?sqp=-oaymwEbCKgBEF5IVfKriqkDDggBFQAAiEIYAXABwAEG&rs=AOn4CLAh7_Ngy7E6wjIflEM5jIiP8bF7uA" alt="" />
                        </div>
                        <div className="troley">
                            <img src="https://etanee.id/img/icon/ic_cart_white.svg" alt="" />
                            <div className="count">{this.state.order}</div>
                        </div>
                    </div>
                </div>
                <CardProduct OnCounterChange={(value) => this.handleCounterChange(value)}/>
            </Fragment>
        );
    }
}
export default product;