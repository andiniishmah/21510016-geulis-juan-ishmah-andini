import React, { Fragment,Component } from 'react';

class cardProduct extends Component {
    state = {
        order: 4
    }

    handleCounterChange = (newValue) => {
        this.props.OnCounterChange(newValue)
    }
    handlePlus = () => {
        this.setState({
            order: this.state.order + 1
        }, () => {
            this.handleCounterChange(this.state.order) 
        })
        
    }
    handleMinus = () => {
        this.setState({
            order: this.state.order - 1
        })
    }
    render () {
        return (
            <Fragment>
                <div className="card">
                    <div className="img-thumb-prod">
                        <img src="" alt="" />
                        <p className="product-title">Daging Sapi bumbu rendang</p>
                        <p className="product-price">Rp 300,000</p>
                        <p className="counter"></p>
                        <button className="minus" onClick={this.handleMinus}>-</button>
                        <input type="text" value={this.state.order}/>
                        <button className="plus" onClick={this.handlePlus}>+</button>

                    </div>
                </div>
            </Fragment>
        );
    }
}
export default cardProduct;