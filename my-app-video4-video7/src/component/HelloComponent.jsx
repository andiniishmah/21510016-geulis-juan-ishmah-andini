import React from 'react';
import './HelloComponent.css'

const HelloComponents = () => {
    return <p className="text">Hello Functional Components</p>
  }

export default HelloComponents;