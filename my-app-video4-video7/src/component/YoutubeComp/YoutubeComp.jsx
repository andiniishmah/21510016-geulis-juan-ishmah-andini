import React from 'react';
import './YoutubeComp.css'

const YoutubeComp = (props) => {
    return (
        <div className="youtube-wrapper">
            <div className="img-thumb">
                <img src="https://i.ytimg.com/vi/AYb7l6XDlPo/hqdefault.jpg?sqp=-oaymwEbCKgBEF5IVfKriqkDDggBFQAAiEIYAXABwAEG&rs=AOn4CLBn_ZYLEh1GwoWieItgGZYJtBlP_g" alt="" />
                <p className="time">{props.time}</p>
            </div>
            <p className="title">{props.title}</p>
            <p className="desc">{props.desc}</p>
        </div>
    )
}
YoutubeComp.defaultProps = {
    time : "00.00",
    title :"default title",
    desc : "0x ditonton. 0 hari yang lalu"
}
export default YoutubeComp;